# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic import ListView, DetailView

from .models import Matchs


class MatchList(ListView):
    model = Matchs

    def get_queryset(self):
        return Matchs.objects.all().prefetch_related('players', 'current_map', 'team_a', 'team_b')


class MatchDetail(DetailView):
    model = Matchs

    def get_queryset(self):
        return Matchs.objects.all().prefetch_related('players', 'current_map', 'team_a', 'team_b')


class LatestMatchDetail(DetailView):
    model = Matchs

    def get_object(self, queryset=None):
        return Matchs.objects.filter(status__gte=3,).latest('id')


class MatchScores(ListView):
    template_name = 'api/scores.html'

    def get_queryset(self):
        return Matchs.objects.filter(season__id=4)
