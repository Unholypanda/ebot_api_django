# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Advertising(models.Model):
    id = models.BigAutoField(primary_key=True)
    season = models.ForeignKey('Seasons', models.DO_NOTHING, blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    active = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'advertising'


class Configs(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=50, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'configs'


class DamageReports(models.Model):
    id = models.BigAutoField(primary_key=True)
    match = models.ForeignKey('Matchs', models.DO_NOTHING)
    player = models.ForeignKey('Players', models.DO_NOTHING, related_name="damage_done_reports")
    enemy = models.ForeignKey('Players', models.DO_NOTHING, related_name="damage_taken_reports")
    damage = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'damage_reports'


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class Maps(models.Model):
    id = models.BigAutoField(primary_key=True)
    match = models.ForeignKey('Matchs', models.DO_NOTHING)
    map_name = models.CharField(max_length=50, blank=True, null=True)
    score_1 = models.BigIntegerField(blank=True, null=True)
    score_2 = models.BigIntegerField(blank=True, null=True)
    current_side = models.CharField(max_length=255, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    maps_for = models.CharField(max_length=255, blank=True, null=True)
    nb_ot = models.BigIntegerField(blank=True, null=True)
    identifier_id = models.BigIntegerField(blank=True, null=True)
    tv_record_file = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'maps'


class MapsScore(models.Model):
    id = models.BigAutoField(primary_key=True)
    map = models.ForeignKey(Maps, models.DO_NOTHING)
    type_score = models.CharField(max_length=255, blank=True, null=True)
    score1_side1 = models.BigIntegerField(blank=True, null=True)
    score1_side2 = models.BigIntegerField(blank=True, null=True)
    score2_side1 = models.BigIntegerField(blank=True, null=True)
    score2_side2 = models.BigIntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'maps_score'


class Matchs(models.Model):
    id = models.BigAutoField(primary_key=True)
    ip = models.CharField(max_length=50, blank=True, null=True)
    server = models.ForeignKey('Servers', models.DO_NOTHING, blank=True, null=True)
    season = models.ForeignKey('Seasons', models.DO_NOTHING, blank=True, null=True)
    team_a = models.ForeignKey('Teams', models.DO_NOTHING, db_column='team_a', blank=True, null=True, related_name="team_a")
    team_a_flag = models.CharField(max_length=2, blank=True, null=True)
    team_a_name = models.CharField(max_length=25, blank=True, null=True)
    team_b = models.ForeignKey('Teams', models.DO_NOTHING, db_column='team_b', blank=True, null=True, related_name="team_b")
    team_b_flag = models.CharField(max_length=2, blank=True, null=True)
    team_b_name = models.CharField(max_length=25, blank=True, null=True)
    status = models.SmallIntegerField(blank=True, null=True)
    is_paused = models.IntegerField(blank=True, null=True)
    score_a = models.BigIntegerField(blank=True, null=True)
    score_b = models.BigIntegerField(blank=True, null=True)
    max_round = models.IntegerField()
    rules = models.CharField(max_length=200)
    overtime_startmoney = models.BigIntegerField(blank=True, null=True)
    overtime_max_round = models.IntegerField(blank=True, null=True)
    config_full_score = models.IntegerField(blank=True, null=True)
    config_ot = models.IntegerField(blank=True, null=True)
    config_streamer = models.IntegerField(blank=True, null=True)
    config_knife_round = models.IntegerField(blank=True, null=True)
    config_switch_auto = models.IntegerField(blank=True, null=True)
    config_auto_change_password = models.IntegerField(blank=True, null=True)
    config_password = models.CharField(max_length=50, blank=True, null=True)
    config_heatmap = models.IntegerField(blank=True, null=True)
    config_authkey = models.CharField(max_length=200, blank=True, null=True)
    enable = models.IntegerField(blank=True, null=True)
    map_selection_mode = models.CharField(max_length=255, blank=True, null=True)
    ingame_enable = models.IntegerField(blank=True, null=True)
    current_map = models.ForeignKey(Maps, models.DO_NOTHING, db_column='current_map', blank=True, null=True)
    force_zoom_match = models.IntegerField(blank=True, null=True)
    identifier_id = models.CharField(max_length=100, blank=True, null=True)
    startdate = models.DateTimeField(blank=True, null=True)
    auto_start = models.IntegerField(blank=True, null=True)
    auto_start_time = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'matchs'
        ordering = ['-id']


class PlayerKill(models.Model):
    id = models.BigAutoField(primary_key=True)
    match = models.ForeignKey(Matchs, models.DO_NOTHING)
    map = models.ForeignKey(Maps, models.DO_NOTHING)
    killer_name = models.CharField(max_length=100, blank=True, null=True)
    killer = models.ForeignKey('Players', models.DO_NOTHING, blank=True, null=True, related_name='kills')
    killer_team = models.CharField(max_length=20, blank=True, null=True)
    killed_name = models.CharField(max_length=100, blank=True, null=True)
    killed = models.ForeignKey('Players', models.DO_NOTHING, blank=True, null=True, related_name='deaths')
    killed_team = models.CharField(max_length=20, blank=True, null=True)
    weapon = models.CharField(max_length=100, blank=True, null=True)
    headshot = models.IntegerField(blank=True, null=True)
    round_id = models.BigIntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'player_kill'


class Players(models.Model):
    id = models.BigAutoField(primary_key=True)
    match = models.ForeignKey(Matchs, models.DO_NOTHING,  related_name='players')
    map = models.ForeignKey(Maps, models.DO_NOTHING)
    player_key = models.CharField(max_length=255, blank=True, null=True)
    team = models.CharField(max_length=255, blank=True, null=True)
    ip = models.CharField(max_length=255, blank=True, null=True)
    steamid = models.CharField(max_length=255, blank=True, null=True)
    first_side = models.CharField(max_length=255, blank=True, null=True)
    current_side = models.CharField(max_length=255, blank=True, null=True)
    pseudo = models.CharField(max_length=255, blank=True, null=True)
    nb_kill = models.BigIntegerField(blank=True, null=True)
    assist = models.BigIntegerField(blank=True, null=True)
    death = models.BigIntegerField(blank=True, null=True)
    point = models.BigIntegerField(blank=True, null=True)
    hs = models.BigIntegerField(blank=True, null=True)
    defuse = models.BigIntegerField(blank=True, null=True)
    bombe = models.BigIntegerField(blank=True, null=True)
    tk = models.BigIntegerField(blank=True, null=True)
    nb1 = models.BigIntegerField(blank=True, null=True)
    nb2 = models.BigIntegerField(blank=True, null=True)
    nb3 = models.BigIntegerField(blank=True, null=True)
    nb4 = models.BigIntegerField(blank=True, null=True)
    nb5 = models.BigIntegerField(blank=True, null=True)
    nb1kill = models.BigIntegerField(blank=True, null=True)
    nb2kill = models.BigIntegerField(blank=True, null=True)
    nb3kill = models.BigIntegerField(blank=True, null=True)
    nb4kill = models.BigIntegerField(blank=True, null=True)
    nb5kill = models.BigIntegerField(blank=True, null=True)
    pluskill = models.BigIntegerField(blank=True, null=True)
    firstkill = models.BigIntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    @property
    def total_rounds(self):
        return self.match.score_a + self.match.score_b

    @property
    def adr(self):
        damage = 0
        rounds = self.match.score_a + self.match.score_b
        if rounds == 0:
            return 0
        else:
            for report in self.damage_done_reports.all():
                damage += report.damage
            return damage / float(rounds)

    @property
    def kdr(self):
        if self.nb_kill == 0:
            return 0.00
        elif self.death == 0:
            return self.nb_kill
        else:
            return self.nb_kill / float(self.death)

    @property
    def fpr(self):
        rounds = self.match.score_a + self.match.score_b
        if self.nb_kill == 0 or rounds == 0:
            return 0
        else:
            return self.nb_kill / float(rounds)

    @property
    def hsp(self):
        if self.nb_kill == 0 or self.hs == 0:
            return 0
        else:
            return self.hs / float(self.nb_kill)

    class Meta:
        managed = False
        db_table = 'players'
        ordering = ['-nb_kill']


class PlayersHeatmap(models.Model):
    id = models.BigAutoField(primary_key=True)
    match = models.ForeignKey(Matchs, models.DO_NOTHING)
    map = models.ForeignKey(Maps, models.DO_NOTHING)
    event_name = models.CharField(max_length=50, blank=True, null=True)
    event_x = models.FloatField(blank=True, null=True)
    event_y = models.FloatField(blank=True, null=True)
    event_z = models.FloatField(blank=True, null=True)
    player_name = models.CharField(max_length=255, blank=True, null=True)
    player = models.ForeignKey(Players, models.DO_NOTHING, blank=True, null=True, related_name="heatmap_player")
    player_team = models.CharField(max_length=20, blank=True, null=True)
    attacker_x = models.FloatField(blank=True, null=True)
    attacker_y = models.FloatField(blank=True, null=True)
    attacker_z = models.FloatField(blank=True, null=True)
    attacker_name = models.CharField(max_length=255, blank=True, null=True)
    attacker = models.ForeignKey(Players, models.DO_NOTHING, blank=True, null=True, related_name="heatmap_attacker")
    attacker_team = models.CharField(max_length=20, blank=True, null=True)
    round_id = models.BigIntegerField(blank=True, null=True)
    round_time = models.BigIntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'players_heatmap'


class PlayersSnapshot(models.Model):
    id = models.BigAutoField(primary_key=True)
    player = models.ForeignKey(Players, models.DO_NOTHING)
    player_key = models.CharField(max_length=255, blank=True, null=True)
    first_side = models.CharField(max_length=255, blank=True, null=True)
    current_side = models.CharField(max_length=255, blank=True, null=True)
    nb_kill = models.BigIntegerField(blank=True, null=True)
    assist = models.BigIntegerField(blank=True, null=True)
    death = models.BigIntegerField(blank=True, null=True)
    point = models.BigIntegerField(blank=True, null=True)
    hs = models.BigIntegerField(blank=True, null=True)
    defuse = models.BigIntegerField(blank=True, null=True)
    bombe = models.BigIntegerField(blank=True, null=True)
    tk = models.BigIntegerField(blank=True, null=True)
    nb1 = models.BigIntegerField(blank=True, null=True)
    nb2 = models.BigIntegerField(blank=True, null=True)
    nb3 = models.BigIntegerField(blank=True, null=True)
    nb4 = models.BigIntegerField(blank=True, null=True)
    nb5 = models.BigIntegerField(blank=True, null=True)
    nb1kill = models.BigIntegerField(blank=True, null=True)
    nb2kill = models.BigIntegerField(blank=True, null=True)
    nb3kill = models.BigIntegerField(blank=True, null=True)
    nb4kill = models.BigIntegerField(blank=True, null=True)
    nb5kill = models.BigIntegerField(blank=True, null=True)
    pluskill = models.BigIntegerField(blank=True, null=True)
    firstkill = models.BigIntegerField(blank=True, null=True)
    round_id = models.BigIntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'players_snapshot'


class Round(models.Model):
    id = models.BigAutoField(primary_key=True)
    match = models.ForeignKey(Matchs, models.DO_NOTHING)
    map = models.ForeignKey(Maps, models.DO_NOTHING)
    event_name = models.CharField(max_length=255, blank=True, null=True)
    event_text = models.TextField(blank=True, null=True)
    event_time = models.BigIntegerField(blank=True, null=True)
    kill = models.ForeignKey(PlayerKill, models.DO_NOTHING, blank=True, null=True)
    round_id = models.BigIntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'round'


class RoundSummary(models.Model):
    id = models.BigAutoField(primary_key=True)
    match = models.ForeignKey(Matchs, models.DO_NOTHING)
    map = models.ForeignKey(Maps, models.DO_NOTHING)
    bomb_planted = models.IntegerField(blank=True, null=True)
    bomb_defused = models.IntegerField(blank=True, null=True)
    bomb_exploded = models.IntegerField(blank=True, null=True)
    win_type = models.CharField(max_length=255, blank=True, null=True)
    team_win = models.CharField(max_length=255, blank=True, null=True)
    ct_win = models.IntegerField(blank=True, null=True)
    t_win = models.IntegerField(blank=True, null=True)
    score_a = models.SmallIntegerField(blank=True, null=True)
    score_b = models.SmallIntegerField(blank=True, null=True)
    best_killer = models.ForeignKey(Players, models.DO_NOTHING, db_column='best_killer', blank=True, null=True)
    best_killer_nb = models.BigIntegerField(blank=True, null=True)
    best_killer_fk = models.IntegerField(blank=True, null=True)
    best_action_type = models.TextField(blank=True, null=True)
    best_action_param = models.TextField(blank=True, null=True)
    backup_file_name = models.CharField(max_length=255, blank=True, null=True)
    round_id = models.BigIntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'round_summary'


class Seasons(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=50)
    event = models.CharField(max_length=50)
    start = models.DateTimeField()
    end = models.DateTimeField()
    link = models.CharField(max_length=100, blank=True, null=True)
    logo = models.CharField(max_length=255, blank=True, null=True)
    active = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'seasons'


class Servers(models.Model):
    id = models.BigAutoField(primary_key=True)
    ip = models.CharField(max_length=50)
    rcon = models.CharField(max_length=50)
    hostname = models.CharField(max_length=100)
    tv_ip = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'servers'


class SfGuardForgotPassword(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey('SfGuardUser', models.DO_NOTHING)
    unique_key = models.CharField(max_length=255, blank=True, null=True)
    expires_at = models.DateTimeField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'sf_guard_forgot_password'


class SfGuardGroup(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'sf_guard_group'


class SfGuardGroupPermission(models.Model):
    group = models.ForeignKey(SfGuardGroup, models.DO_NOTHING, primary_key=True)
    permission = models.ForeignKey('SfGuardPermission', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'sf_guard_group_permission'
        unique_together = (('group', 'permission'),)


class SfGuardPermission(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'sf_guard_permission'


class SfGuardRememberKey(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey('SfGuardUser', models.DO_NOTHING, blank=True, null=True)
    remember_key = models.CharField(max_length=32, blank=True, null=True)
    ip_address = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'sf_guard_remember_key'


class SfGuardUser(models.Model):
    id = models.BigAutoField(primary_key=True)
    first_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)
    email_address = models.CharField(unique=True, max_length=255)
    username = models.CharField(unique=True, max_length=128)
    algorithm = models.CharField(max_length=128)
    salt = models.CharField(max_length=128, blank=True, null=True)
    password = models.CharField(max_length=128, blank=True, null=True)
    is_active = models.IntegerField(blank=True, null=True)
    is_super_admin = models.IntegerField(blank=True, null=True)
    last_login = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'sf_guard_user'


class SfGuardUserGroup(models.Model):
    user = models.ForeignKey(SfGuardUser, models.DO_NOTHING, primary_key=True)
    group = models.ForeignKey(SfGuardGroup, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'sf_guard_user_group'
        unique_together = (('user', 'group'),)


class SfGuardUserPermission(models.Model):
    user = models.ForeignKey(SfGuardUser, models.DO_NOTHING, primary_key=True)
    permission = models.ForeignKey(SfGuardPermission, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'sf_guard_user_permission'
        unique_together = (('user', 'permission'),)


class Teams(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=50)
    shorthandle = models.CharField(max_length=50)
    flag = models.CharField(max_length=2)
    link = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'teams'


class TeamsInSeasons(models.Model):
    id = models.BigAutoField(primary_key=True)
    season = models.ForeignKey(Seasons, models.DO_NOTHING, blank=True, null=True)
    team = models.ForeignKey(Teams, models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'teams_in_seasons'
